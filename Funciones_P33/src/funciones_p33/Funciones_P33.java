/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package funciones_p33;

import java.util.Scanner;

/**
 *
 * @author droa
 */
public class Funciones_P33 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner leer= new Scanner(System.in);
        System.out.println("Escribir Primer Número:");
        int num1 = leer.nextInt();
        System.out.println("Escribir Segundo Número:");
        int num2 = leer.nextInt();
        EsMultiplo(num1,num2);
        
    }
    public static void EsMultiplo(int n1,int n2){
        
        
        if((n1%n2) == 0 ){
            System.out.println("El número "+n1+" es múltiplo de "+n2+".");
        }else{
             System.out.println("El número "+n1+" no es múltiplo de "+n2+".");
        }
    
    }
    
    
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package ejercicio_14;

import java.util.Scanner;

/**
 *
 * @author droa
 */
public class Ejercicio_14 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner leer= new Scanner(System.in);
        
        System.out.println("Ingrese la cantidad de Euros a convertir: ");
        
        int l=4;
        
        float[] valores_monedas = new float[l];
        
        // indice 0 -> EUR
        // indice 1 -> USD
        // indice 2 -> JPY
        // indice 3 -> GBP
        
        valores_monedas[0]=leer.nextFloat();

        
        for(int i = 0;i < 4; i++) {    
            switch (i) {
                case 0:
                    break;
                case 1:
                    valores_monedas[i]=EUR2USD(valores_monedas[0]);
                    System.out.println(valores_monedas[0]+" EUR son "+ valores_monedas[i] + " USD");                    
                    break;
                case 2:
                    valores_monedas[i]=EUR2JPY(valores_monedas[0]);
                    System.out.println(valores_monedas[0]+" EUR son "+ valores_monedas[i] + " JPY");
                    break;
                case 3:
                    valores_monedas[i]=EUR2GBP(valores_monedas[0]);
                    System.out.println(valores_monedas[0]+" EUR son "+ valores_monedas[i] + " GBP");
                    break;
                default:  
            }
        }
    }
    
    public static float EUR2USD(float euro){
        
       float USD=euro*1.04f;        
       return USD;
    
    }
    
    public static float EUR2JPY(float euro){
        
        float JPY=euro*144.24f;        
        return JPY;
    
    }
    public static float EUR2GBP(float euro){
        
        float GBP=euro*0.86f;        
        return GBP;
    }
    
    
    
}

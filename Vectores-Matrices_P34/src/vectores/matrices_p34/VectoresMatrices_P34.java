/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package vectores.matrices_p34;

import java.util.Scanner;

/**
 *
 * @author droa
 */
public class VectoresMatrices_P34 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Scanner leer = new Scanner(System.in);
        System.out.println("Ingrese la cantidad de Compañeros: ");
        
        int companieros = leer.nextInt();
        
        String[] equipo = new String[companieros];
        
        for(int i=0;i<companieros;i++){
            System.out.println("Ingrese un nombre: ");
            equipo[i] = leer.next();
        }
        
    }
    
}
